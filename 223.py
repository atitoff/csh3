from CSHLib import ConsoleWriter
from console_writer_settings import Settings

js = """{
  "9": {
    "events": { "701": [0, 30]},
    "active_level": 1,
    "default_value": 50,
    "open_drain": 0,
    "sp_type": 101
  },
  "10": {
    "events": { "701": [0, 30]},
    "active_level": 1,
    "default_value": 50,
    "open_drain": 0,
    "sp_type": 101
  },
  "15": {
    "events": { "700": [0]},
    "active_level": 1,
    "default_value": 1,
    "open_drain": 0,
    "sp_type": 100
  },
  "16": {
    "events": { "700": [0]},
    "active_level": 1,
    "default_value": 1,
    "open_drain": 0,
    "sp_type": 100
  },
  "17": {
    "events": { "701": [0, 30]},
    "active_level": 1,
    "default_value": 50,
    "open_drain": 0,
    "sp_type": 101
  }
}"""


console = ConsoleWriter.ConsoleWriter(Settings, js)
