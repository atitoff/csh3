from CSHLib import ConsoleWriter
from console_writer_settings import Settings

js = """{
  "17": {
    "events": { "700": [0, 30]},
    "active_level": 0,
    "default_value": 50,
    "open_drain": 0,
    "sp_type": 101
  }
}"""


console = ConsoleWriter.ConsoleWriter(Settings, js)
