import json
import os

from jinja2 import Template, Environment, FileSystemLoader


class Board:
    """
    settings in board.h
    """

    def __init__(self):
        self._port_type = {
            0: [8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 4, 15, 8, 8, 8],  # GPIOA
            1: [8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8],  # GPIOB
            2: [8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 6, 8, 8],  # GPIOC
            3: [4, 4, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8]  # GPIOD
        }
        self._odr = {
            0: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],  # GPIOA
            1: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],  # GPIOB
            2: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],  # GPIOC
            3: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]  # GPIOD
        }

    def port_set(self, gpio: int, pin: int, port_type: int, odr: int = 1):
        if port_type > 15 or port_type < 0:
            raise ValueError('0 <= port_type <= 15')
        if gpio > 3 or gpio < 0:
            raise ValueError('0 <= gpio <= 3')
        if pin > 15 or pin < 0:
            raise ValueError('0 <= pin <= 15')
        if odr > 1 or odr < 0:
            raise ValueError('0 <= odr <= 1')
        self._port_type[gpio][pin] = port_type
        self._odr[gpio][pin] = odr

    def _odr2hex_str(self, gpio: int) -> str:
        odr = self._odr[gpio][::-1]
        return 'FFFF{:04X}'.format(int(''.join(map(str, odr)), 2))

    def _port2hex_str(self, gpio: int) -> list:
        def hex_letter(i):
            return hex(i)[2].upper()

        hex_str = ''.join(map(hex_letter, self._port_type[gpio]))
        return [hex_str[0:8][::-1], hex_str[8:][::-1]]

    def generate(self):
        return {
            'GPIOACRL': self._port2hex_str(0)[0],
            'GPIOACRH': self._port2hex_str(0)[1],
            'GPIOBCRL': self._port2hex_str(1)[0],
            'GPIOBCRH': self._port2hex_str(1)[1],
            'GPIOCCRL': self._port2hex_str(2)[0],
            'GPIOCCRH': self._port2hex_str(2)[1],
            'GPIODCRL': self._port2hex_str(3)[0],
            'GPIODCRH': self._port2hex_str(3)[1],
            'GPIOAODR': self._odr2hex_str(0),
            'GPIOBODR': self._odr2hex_str(1),
            'GPIOCODR': self._odr2hex_str(2),
            'GPIODODR': self._odr2hex_str(3)
        }

    # def __del__(self):
    #     print('\n\rboard\n\rA:', self._port2hex_str(0), self._odr2hex_str(0))
    #     print('B:', self._port2hex_str(1), self._odr2hex_str(1))
    #     print('C:', self._port2hex_str(2), self._odr2hex_str(2))
    #     print('D:', self._port2hex_str(3), self._odr2hex_str(3))


class CanModule:
    start_address_0 = 0x08003000
    start_address_1 = 0x08009400

    def __init__(self, c_project_path, ldscript_path, bank, module_id, serial_debug=0):
        self.c_project_path = c_project_path
        self.ldscript_path = ldscript_path
        self.bank = bank
        self.module_id = module_id
        self.serial_debug = serial_debug
        self.errors = []
        # set jinja2 environment
        self.tmpl_env = Environment(
            loader=FileSystemLoader(os.path.join(self.c_project_path, 'template'))
        )

        self.sp_classes = {  # {sp_type: sp_class}
            1: SpGpioDigitalInput,
            10: SpOneWireThermalSensor,
            100: SpGpioDigitalOutput,
            101: SpGpioPWMOutput
        }

        self.ports = {}  # port: sp_class
        self.board = Board()
        # template variable
        self.subscribers_events = []
        self.template = {
            'publicators': {},
            'subscribers': {},
            'board': {},
            'halconf': {'HAL_USE_PWM': 'FALSE'}
        }

    @staticmethod
    def dict_key_to_int(in_dict: dict):
        out_dict = {}
        for key, val in in_dict.items():
            out_dict[int(key)] = val
        return out_dict

    @property
    def gpio_ports_map(self):
        """
        Return ports map [[GPIO, PIN], ...]
        :return:
        """
        return [
            [None, None],
            # 1       2       3       4       5       6
            [0, 0], [0, 1], [0, 2], [0, 3], [0, 8], [0, 15],
            # 7       8       9       10      11      12
            [0, 4], [0, 5], [0, 6], [0, 7], [0, 14], [0, 13],
            # 13      14      15      16      17      18
            [1, 0], [1, 1], [1, 6], [1, 7], [1, 8], [1, 9]
        ]

    def gpio_port_name(self, port):
        return [
            'GPIO' + chr(65 + self.gpio_ports_map[port][0]),
            chr(65 + self.gpio_ports_map[port][0]),
            self.gpio_ports_map[port][1]
        ]

    @property
    def list_possible_gpio_ports(self):
        ret = {}
        for gpio_port in range(1, len(self.gpio_ports_map) + 1):
            av_sp = []
            for sp_type, sp_class in self.sp_classes.items():
                try:  # only gpio sp
                    if gpio_port in sp_class.available_ports:
                        av_sp.append(sp_type)
                except AttributeError:
                    continue
            ret[gpio_port] = av_sp
        return ret

    def sp_name(self, sp_type):
        try:
            return self.sp_classes[sp_type].name
        except Exception:
            return ''

    def add_sp(self, json_str):

        for port, value in json.loads(json_str).items():
            try:
                port = int(port)
            except ValueError:
                self.errors.append('Error in port number. Port {} not append'.format(port))
                continue
            if value['sp_type'] == 100:
                validate = self.sp_classes[100].validate(value)
                if validate is not None:  # validate value
                    self.errors.append('Error in value {}. Port {} not append'.format(validate, port))
                    continue
                if port not in self.sp_classes[100].available_ports:  # if available GPIO port
                    self.errors.append('Error. Port {} not available'.format(port))
                    continue
                # add SP
                if SpGpioDigitalOutput.can_module_class is None:
                    SpGpioDigitalOutput.can_module_class = self
                self.ports[port] = self.sp_classes[100](
                    can_module=self,
                    port=port,
                    events=self.dict_key_to_int(value['events']),
                    open_drain=value['open_drain'],
                    active_level=value['active_level'],
                    default_value=value['default_value']
                )
            if value['sp_type'] == 101:
                validate = self.sp_classes[101].validate(value)
                if validate is not None:  # validate value
                    self.errors.append('Error in value {}. Port {} not append'.format(validate, port))
                    continue
                if port not in self.sp_classes[101].available_ports:  # if available GPIO port
                    self.errors.append('Error. Port {} not available'.format(port))
                    continue
                # add SP
                self.ports[port] = self.sp_classes[101](
                    can_module=self,
                    port=port,
                    events=self.dict_key_to_int(value['events']),
                    open_drain=value['open_drain'],
                    active_level=value['active_level'],
                    default_value=value['default_value']
                )
            if value['sp_type'] == 1:
                validate = self.sp_classes[1].validate(value)
                if validate is not None:  # validate value
                    self.errors.append('Error in value {}. Port {} not append'.format(validate, port))
                    continue
                if port not in self.sp_classes[1].available_ports:  # if available GPIO port
                    self.errors.append('Error. Port {} not available'.format(port))
                    continue
                # add SP
                if SpGpioDigitalInput.can_module_class is None:
                    SpGpioDigitalInput.can_module_class = self
                self.ports[port] = self.sp_classes[1](
                    port=port,
                    events=self.dict_key_to_int(value['events']),
                    pull=value['pull'],
                    active_level=value['active_level']
                )

    def gen_sp_100_digital_output(self, event, sp_class):
        if event > 0:
            event_data = sp_class.events[event]
            port_name = self.gpio_port_name(sp_class.port)[0]
            pin = self.gpio_ports_map[sp_class.port][1]
            if len(event_data) == 1:
                tmpl = Template(
                    """  set_gpio_port({{port}}, CanReceive.databit, {{active_level}});"""
                )
                if (event_data[0] == 1 and sp_class.active_level == 1) or (
                        event_data[0] == 0 and sp_class.active_level == 0):
                    active_level = 1
                else:
                    active_level = 0
                self.subscribers_events.append(tmpl.render(port=sp_class.port, active_level=active_level))

    def gen_sp_101_pwm_output(self, event, sp_class):
        # init all pwm_output
        if event == 0:
            ret_all = {'pwmcfg': [], 'pwm_start': [], 'pwm_default': []}
            used_gpio = {}  # port: [gpio, pin]
            for port, sp_class in self.ports.items():
                if sp_class.sp_type == 101:
                    self.template['halconf']['HAL_USE_PWM'] = 'TRUE'
                    used_gpio[port] = self.gpio_ports_map[port]

            tim3_gpio = [[0, 6], [0, 7], [1, 0], [1, 1]]
            tim4_gpio = [[1, 6], [1, 7], [1, 8], [1, 9]]
            tim3 = ['PWM_OUTPUT_DISABLED'] * 4
            tim3_f = 0
            tim4 = tim3[:]
            tim4_f = 0
            for port, gpio in used_gpio.items():
                for i, t_gpio in enumerate(tim3_gpio):
                    if gpio == t_gpio:
                        if self.ports[port].active_level:
                            tim3[i] = 'PWM_OUTPUT_ACTIVE_HIGH'
                        else:
                            tim3[i] = 'PWM_OUTPUT_ACTIVE_LOW'
                        tim3_f = 1
                for i, t_gpio in enumerate(tim4_gpio):
                    if gpio == t_gpio:
                        if self.ports[port].active_level:
                            tim4[i] = 'PWM_OUTPUT_ACTIVE_HIGH'
                        else:
                            tim4[i] = 'PWM_OUTPUT_ACTIVE_LOW'
                        tim4_f = 1
            # add to define subscribers
            cfg = Template("""static PWMConfig pwmcfg{{cfg_n}} = {
  10000,                            /* 10kHz PWM clock frequency.   */
  100,                              /* Initial PWM period 10 mS    */
  NULL,
  {
   { {{port[0]}}, NULL},    // CH1 A6 or B6
   { {{port[1]}}, NULL},    // CH2 A7 or B7
   { {{port[2]}}, NULL},    // CH3 B0 or B8
   { {{port[3]}}, NULL}    // CH4 B1 or B9
  },
  0,
  0,
#if STM32_PWM_USE_ADVANCED
  0
#endif
};

""")

            if tim3_f:
                ret_all['pwmcfg'].append(cfg.render(port=tim3, cfg_n=0))
                ret_all['pwm_start'].append('pwmStart(&PWMD3, &pwmcfg0);')
            if tim4_f:
                ret_all['pwmcfg'].append(cfg.render(port=tim4, cfg_n=1))
                ret_all['pwm_start'].append('pwmStart(&PWMD4, &pwmcfg1);')

            # default values
            for port, sp_class in self.ports.items():
                if sp_class.sp_type == 101:
                    ret_all['pwm_default'].append(
                        'pwmEnableChannel(&PWMD{0[0]}, {0[1]}, PWM_PERCENTAGE_TO_WIDTH(&PWMD{0[0]}, {1}));'.format(
                            sp_class.channels[sp_class.port], sp_class.default_value * 100
                        )
                    )

            return ret_all

        #  generate data for single port
        else:
            self.subscribers_events.append('  set_pwm_port({}, {}, {}, {});'.format(
                sp_class.channels[sp_class.port][0],
                sp_class.channels[sp_class.port][1],
                sp_class.events[event][0],
                sp_class.events[event][1]
            ))

    def gen_subscribers(self):
        subscribers_sp_type = [100, 101]
        gen = {
            100: self.gen_sp_100_digital_output,
            101: self.gen_sp_101_pwm_output
        }
        events = {}  # for ex. {715: [sp_class, sp_class], 716: [sp_class]}
        for port, sp_class in self.ports.items():  # обегаем все софт-порты
            if sp_class.sp_type in subscribers_sp_type:  # выбираем только подписчиков
                for event in sp_class.events:
                    try:
                        if sp_class.sp_type not in events[event]:
                            events[event].append(sp_class)
                    except KeyError:
                        events[event] = []
                        events[event].append(sp_class)
        first_event_f = True
        for event in sorted(events):
            if first_event_f:
                self.subscribers_events.append('\n\r// Event processing')
                self.subscribers_events.append('if (CanReceive.event == {}){{'.format(event))
                first_event_f = False
            else:
                self.subscribers_events.append('else if (CanReceive.event == {}){{'.format(event))

            for sp_class in events[event]:
                # генерируем данные для каждого порта по принимаемому событию
                gen[sp_class.sp_type](event, sp_class)

            self.subscribers_events.append('}')

        # генерируем групповые данные для каждого типа софт-порта
        for sp_type in subscribers_sp_type:
            # gen[sp_type](0)
            # print('group generate for sp_type:', sp_type)
            pass

        if len(self.subscribers_events):
            return {'subscriber_strings': '\n\r'.join(self.subscribers_events)}
        else:
            return {'subscriber_strings': ''}

    def generate(self):
        # region ### publicators.c ###
        publicators = SpGpioDigitalInput.generate()  # {'timer20ms': '', 'init_timer20ms': ''}
        with open(os.path.join(self.c_project_path, 'publicators.c'), 'w') as f:
            tmpl = self.tmpl_env.get_template('publicators.tpl.c')
            f.write(
                tmpl.render(**publicators)
            )
        # endregion

        # region ### subscribers.c ###
        subscribers = self.gen_subscribers()  # {'subscriber_strings': ''}
        sb1: dict = self.gen_sp_101_pwm_output(0, 0)  # group data 101
        subscribers.update(sb1)
        if self.template['halconf']['HAL_USE_PWM'] == 'TRUE':
            subscribers.update({'HAL_USE_PWM': True})
        with open(os.path.join(self.c_project_path, 'subscribers.c'), 'w') as f:
            tmpl = self.tmpl_env.get_template('subscribers.tpl.c')
            f.write(
                tmpl.render(**subscribers)
            )
        with open(os.path.join(self.c_project_path, 'subscribers.h'), 'w') as f:
            tmpl = self.tmpl_env.get_template('subscribers.tpl.h')
            f.write(
                tmpl.render(**subscribers)
            )
        # endregion

        # region ### board.h ###
        board = self.board.generate()  # {'GPIOACRL': '8F888888', ..
        with open(os.path.join(self.c_project_path, 'board', 'board.h'), 'w') as f:
            tmpl = self.tmpl_env.get_template('board.tpl.h')
            f.write(
                tmpl.render(**board)
            )
        # endregion

        # region ### halconf.h ###
        with open(os.path.join(self.c_project_path, 'halconf.h'), 'w') as f:
            tmpl = self.tmpl_env.get_template('halconf.production.h')
            f.write(
                tmpl.render(**self.template['halconf'])
            )
        # endregion

        # region ### ldscript ###
        if self.bank > 0:
            self.bank = 1
            start_address = self.start_address_1
        else:
            start_address = self.start_address_0
        ldscript = {
            'org': '0x{:08X}'.format(start_address),
            'len': '- 0x{:X}'.format(start_address - 0x08000000)
        }
        with open(self.ldscript_path, 'w') as f:
            tmpl = self.tmpl_env.get_template('STM32F103x8.tpl.ld')
            f.write(
                tmpl.render(**ldscript)
            )
        # endregion

        # region ### sh_conf.h ###
        sh_conf = {
            'MODULE_ID': self.module_id,
            'FW_CNT': 1,
            'CURRENT_PROGRAM_BANK': self.bank,
            'PROGRAM_BANK_0_START_ADDRESS': '0x{:08X}'.format(self.start_address_0),
            'PROGRAM_BANK_SIZE': 25,
            'PAGE_SIZE': 1024,
            'PROGRAM_BANK_SELECTOR_ADDRESS': '0x0800FFFE',
            'SERIAL_DEBUG': self.serial_debug
        }
        with open(os.path.join(self.c_project_path, 'sh_conf.h'), 'w') as f:
            tmpl = self.tmpl_env.get_template('sh_conf.tpl.h')
            f.write(
                tmpl.render(**sh_conf)
            )
        # endregion

        # region ### chconf.h ###
        ch_dbg_statistic = 'FALSE'
        if self.serial_debug:
            ch_dbg_statistic = 'TRUE'
        chconf = {
            'CORTEX_VTOR_INIT': '0x{:X}'.format(start_address - 0x08000000),
            'CH_DBG_STATISTICS': ch_dbg_statistic
        }
        with open(os.path.join(self.c_project_path, 'chconf.h'), 'w') as f:
            tmpl = self.tmpl_env.get_template('chconf.tpl.h')
            f.write(
                tmpl.render(**chconf)
            )
        # endregion

    def loop_event(self, current_pub_port):
        # print('current publicator port: {}, events: {}'.format(current_pub_port, self.ports[current_pub_port].events))
        for port, sp_class in self.ports.items():
            if sp_class.sp_type == 100:
                # print(sp_class.events)
                for event, val in sp_class.events.items():
                    # print(event)
                    pass

            if sp_class.sp_type == 101:
                # print(sp_class.events)
                pass

    def save(self):
        json_str = {}
        for port, sp_class in self.ports.items():
            json_str[port] = sp_class.save()
        return json.dumps(json_str, sort_keys=True)


class SpGpioDigitalInput:
    name = "Digital input"
    sp_type = 1
    available_ports = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
    can_module_class: CanModule = None  # ссылка на класс-родитель CanModule

    def __init__(self, port, events, active_level, pull):
        self.port = port
        self.active_level = active_level
        self.pull = pull
        self.events = events
        self.board_init()

    def board_init(self):
        gpio, pin = self.can_module_class.gpio_ports_map[self.port]
        port_type = 4
        if self.pull == 1:
            port_type = 8
        pull = 1
        if self.active_level == 1 and self.pull == 1:
            pull = 0
        self.can_module_class.board.port_set(gpio, pin, port_type, pull)

    @classmethod
    def generate(cls):
        buttons = []
        template_main = Template("""
virtual_timer_t timer20ms_timer;

void timer20ms(void *p) {

  /* Restarts the timer.*/
  chSysLockFromISR();
  chVTSetI(&timer20ms_timer, (systime_t)200, timer20ms, p);  // 20 ms
  chSysUnlockFromISR();

  /* Periodic code here.*/

{% for button in buttons %}
  {{button}}
{% endfor %}
}
""")
        for port, sp_class in cls.can_module_class.ports.items():
            if sp_class.sp_type == 1:

                gpio_l, gpio_s, pin = cls.can_module_class.gpio_port_name(port)
                template = Template("""
  /* BUTTON_{{S}}{{PIN}}_START */
  static uint8_t active_cnt_{{PS}};
  static uint8_t unactive_cnt_{{PS}};
  if (palReadPad({{L}}, {{PIN}}) == 0) {
    if (unactive_cnt_{{PS}} == 0) {
      active_cnt_{{PS}}++;
      if (active_cnt_{{PS}} > 3) {
        {% for event in events -%}
        {{event}}; // send can
        {{loop_event}}; // loop event
        {% endfor %}
        unactive_cnt_{{PS}} = 5;
        active_cnt_{{PS}} = 0;
      }
    }
  }
  else {
    active_cnt_{{PS}} = 0;
    if(unactive_cnt_{{PS}} > 0){
      unactive_cnt_{{PS}}--;
    }
  }
  /* BUTTON_{{S}}{{PIN}}_END */""")
                events = []
                for event, val in sp_class.events.items():
                    if len(val) == 1:
                        events.append('can_bus_send({0}, {1[0]})'.format(event, val))
                    if len(val) == 2:
                        events.append('can_bus_send_1b({0}, {1[0]}, {1[1]})'.format(event, val))
                    if 2 < len(val) <= 9:
                        databytes_name = 'event{}_data'.format(event)
                        databytes_value = ', '.join(map(str, val[1:]))
                        databytes = 'const uint8_t ' + databytes_name + ' = {' + databytes_value + '};'
                        events.append(databytes + ' can_bus_send_d({0}, {1}, {2}, {3})'.format(
                            event, val[0], databytes_name, len(val[1:])
                        ))

                # find loop event in subscribers
                cls.can_module_class.loop_event(port)

                buttons.append(
                    template.render(S=gpio_s, L=gpio_l, PIN=pin, PS=gpio_s.lower() + str(pin), events=events)
                )

        if len(buttons):
            return {
                'timer20ms': template_main.render(buttons=buttons),
                'init_timer20ms': """  chVTSet( & timer20ms_timer, MS2ST(1000), timer20ms, NULL);"""
            }
        else:
            return {'timer20ms': '', 'init_timer20ms': ''}

    @staticmethod
    def validate(var):
        try:
            if var['active_level'] not in range(0, 2):
                return 'active_level'
            if var['pull'] not in range(0, 2):
                return 'pull'
            for event, val in var['events'].items():
                try:
                    if int(event) not in range(100, 45056):
                        return 'event'
                except ValueError:
                    return 'event'
                if len(val) == 0 or len(val) > 9:
                    return 'event'
                if val[0] not in range(0, 2):
                    return 'event'
                for item in val[1:]:
                    if item not in range(0, 256):
                        return 'event'
        except KeyError as err:
            return err
        return None

    def save(self):
        ret = {
            'sp_type': self.sp_type,
            'active_level': self.active_level,
            'pull': self.pull,
            'events': self.events
        }
        return ret


class SpGpioDigitalOutput:
    name = "Digital output"
    sp_type = 100
    available_ports = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
    can_module_class: CanModule = None  # ссылка на класс-родитель CanModule

    def __init__(self, can_module: CanModule, port, events, active_level, open_drain, default_value):
        self.can_module = can_module  # ссылка на класс-родитель CanModule
        self.port = port
        self.events = events
        self.active_level = active_level
        self.open_drain = open_drain
        self.default_value = default_value
        self.board_init()

    def board_init(self):
        gpio, pin = self.can_module.gpio_ports_map[self.port]
        port_type = 2
        if self.open_drain:
            port_type = 6
        self.can_module.board.port_set(gpio, pin, port_type, self.default_value)

    @staticmethod
    def validate(var):
        try:
            if var['active_level'] not in range(0, 2):
                return 'active_level'
            if var['default_value'] not in range(0, 2):
                return 'default_value'
            if var['open_drain'] not in range(0, 2):
                return 'open_drain'
            for event, val in var['events'].items():
                try:
                    if int(event) not in range(100, 45056):
                        return 'event'
                except ValueError:
                    return 'event'
                if len(val) != 1:
                    return 'event'
                if val[0] not in range(0, 2):
                    return 'event'
        except KeyError as err:
            return err
        return None

    @classmethod
    def generate(cls):
        print(cls.can_module_class)

    def save(self):
        return {
            'sp_type': self.sp_type,
            'active_level': self.active_level,
            'open_drain': self.open_drain,
            'events': self.events
        }


class SpGpioPWMOutput:
    name = "PWM output"
    sp_type = 101
    available_ports = [9, 10, 13, 14, 15, 16, 17, 18]
    channels = {9: [3, 0], 10: [3, 1], 13: [3, 2], 14: [3, 3], 15: [4, 0], 16: [4, 1], 17: [4, 2], 18: [4, 3]}
    # {port: [timer, timer_ch]}

    def __init__(self, can_module: CanModule, port, events, active_level, default_value, open_drain):
        self.can_module = can_module  # ссылка на класс-родитель CanModule
        self.port = port
        self.events = events
        self.active_level = active_level
        self.default_value = default_value
        self.open_drain = open_drain
        self.board_init()

    def board_init(self):
        gpio, pin = self.can_module.gpio_ports_map[self.port]
        port_type = 0xB
        if self.open_drain:
            port_type = 0xF
        self.can_module.board.port_set(gpio, pin, port_type)

    @staticmethod
    def validate(var):
        try:
            if var['active_level'] not in range(0, 2):
                return 'active_level'
            if var['default_value'] not in range(0, 101):
                return 'default_value'
            if var['open_drain'] not in range(0, 2):
                return 'open_drain'
            for event, val in var['events'].items():
                try:
                    if int(event) not in range(100, 45056):
                        return 'event'
                except ValueError:
                    return 'event'
                if len(val) != 2:
                    return 'event'
                if val[0] not in range(0, 101) or val[1] not in range(0, 101):
                    return 'event'
        except KeyError as err:
            return err
        return None

    def save(self):
        return {
            'sp_type': self.sp_type,
            'active_level': self.active_level,
            'default_value': self.default_value,
            'open_drain': self.open_drain,
            'events': self.events
        }


class SpOneWireThermalSensor:
    name = "One Wire thermal sensor"
    sp_type = 10
    available_ports = range(32, 200)  # [32, 33, ... 199]


if __name__ == "__main__":
    can_module_class = CanModule(
        r'C:\ChibiStudio\workspace176\can_module_online_gen',
        r'C:\ChibiStudio\chibios176\os\common\startup\ARMCMx\compilers\GCC\ld\STM32F103x8_csh.ld',
        0, 0
    )

    can_module_class.add_sp(
        """{
  "1": {
    "events": { "700": [1]},
    "active_level": 0,
    "pull": 1,
    "sp_type": 1
  },
  "2": {
    "events": { "702": [1]},
    "active_level": 0,
    "pull": 1,
    "sp_type": 1
  },
  "3": {
    "events": { "700": [1], "800": [0]},
    "active_level": 0,
    "default_value": 0,
    "open_drain": 1,
    "sp_type": 100
  },
  "18": {
    "active_level": 1,
    "default_value": 0,
    "events": {"129": [0, 100], "800": [1, 33]},
    "open_drain": 1,
    "sp_type": 101
  }
}"""
    )

    if can_module_class.errors:
        print(can_module_class.errors)

    can_module_class.generate()
