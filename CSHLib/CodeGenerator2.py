import json
import os

from jinja2 import Template, Environment, FileSystemLoader


class Board:
    """
    settings in board.h
    """

    def __init__(self):
        self._port_type = {
            0: [8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 4, 15, 8, 8, 8],  # GPIOA
            1: [8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8],  # GPIOB
            2: [8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 6, 8, 8],  # GPIOC
            3: [4, 4, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8]  # GPIOD
        }
        self._odr = {
            0: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],  # GPIOA
            1: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],  # GPIOB
            2: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],  # GPIOC
            3: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]  # GPIOD
        }

    def port_set(self, gpio: int, pin: int, port_type: int, odr: int = 1):
        if port_type > 15 or port_type < 0:
            raise ValueError('0 <= port_type <= 15')
        if gpio > 3 or gpio < 0:
            raise ValueError('0 <= gpio <= 3')
        if pin > 15 or pin < 0:
            raise ValueError('0 <= pin <= 15')
        if odr > 1 or odr < 0:
            raise ValueError('0 <= odr <= 1')
        self._port_type[gpio][pin] = port_type
        self._odr[gpio][pin] = odr

    def _odr2hex_str(self, gpio: int) -> str:
        odr = self._odr[gpio][::-1]
        return 'FFFF{:04X}'.format(int(''.join(map(str, odr)), 2))

    def _port2hex_str(self, gpio: int) -> list:
        def hex_letter(i):
            return hex(i)[2].upper()

        hex_str = ''.join(map(hex_letter, self._port_type[gpio]))
        return [hex_str[0:8][::-1], hex_str[8:][::-1]]

    def generate(self):
        return {
            'GPIOACRL': self._port2hex_str(0)[0],
            'GPIOACRH': self._port2hex_str(0)[1],
            'GPIOBCRL': self._port2hex_str(1)[0],
            'GPIOBCRH': self._port2hex_str(1)[1],
            'GPIOCCRL': self._port2hex_str(2)[0],
            'GPIOCCRH': self._port2hex_str(2)[1],
            'GPIODCRL': self._port2hex_str(3)[0],
            'GPIODCRH': self._port2hex_str(3)[1],
            'GPIOAODR': self._odr2hex_str(0),
            'GPIOBODR': self._odr2hex_str(1),
            'GPIOCODR': self._odr2hex_str(2),
            'GPIODODR': self._odr2hex_str(3)
        }

    # def __del__(self):
    #     print('\n\rboard\n\rA:', self._port2hex_str(0), self._odr2hex_str(0))
    #     print('B:', self._port2hex_str(1), self._odr2hex_str(1))
    #     print('C:', self._port2hex_str(2), self._odr2hex_str(2))
    #     print('D:', self._port2hex_str(3), self._odr2hex_str(3))


class CanModule:
    start_address_0 = 0x08003000
    start_address_1 = 0x08009400

    def __init__(self, c_project_path, ldscript_path, bank, module_id, serial_debug=0):
        self.c_project_path = c_project_path
        self.ldscript_path = ldscript_path
        self.bank = bank
        self.module_id = module_id
        self.serial_debug = serial_debug
        self.ports = {}  # port: sp_class
        self.errors = []
        self.board = Board()
        self.gen_template = {}  # {'template_filename': {template_var: data}}

        # set jinja2 environment
        self.tmpl_env = Environment(
            loader=FileSystemLoader(os.path.join(self.c_project_path, 'template'))
        )

    @staticmethod
    def dict_key_to_int(in_dict: dict):
        out_dict = {}
        for key, val in in_dict.items():
            out_dict[int(key)] = val
        return out_dict

    @property
    def gpio_ports_map(self):
        """
        Return ports map [[GPIO, PIN], ...]
        :return:
        """
        return [
            [None, None],
            # 1       2       3       4       5       6
            [0, 0], [0, 1], [0, 2], [0, 3], [0, 8], [0, 15],
            # 7       8       9       10      11      12
            [0, 4], [0, 5], [0, 6], [0, 7], [0, 14], [0, 13],
            # 13      14      15      16      17      18
            [1, 0], [1, 1], [1, 6], [1, 7], [1, 8], [1, 9]
        ]

    def gpio_port_name(self, port):
        return [
            'GPIO' + chr(65 + self.gpio_ports_map[port][0]),
            chr(65 + self.gpio_ports_map[port][0]),
            self.gpio_ports_map[port][1]
        ]

    def add_template_data(self, data):
        for key, val in data.items():
            if key in self.gen_template:
                self.gen_template[key].update(data[key])
            else:
                self.gen_template.update(data)

    def add_sp(self, json_str):
        for port, value in json.loads(json_str).items():
            try:
                port = int(port)
            except ValueError:
                self.errors.append('Error in port number. Port {} not append'.format(port))
                continue

            if value['sp_type'] == 1:  # SpGpioDigitalInput
                validate = SpGpioDigitalInput.validate(value)
                if validate is not None:  # validate value
                    self.errors.append('Error in value {}. Port {} not append'.format(validate, port))
                    continue
                if port not in SpGpioDigitalInput.available_ports:  # if available GPIO port
                    self.errors.append('Error. Port {} not available'.format(port))
                    continue
                # add SP
                if SpGpioDigitalInput.can_module_class is None:
                    SpGpioDigitalInput.can_module_class = self
                self.ports[port] = SpGpioDigitalInput(
                    port=port,
                    events=self.dict_key_to_int(value['events']),
                    pull=value['pull'],
                    active_level=value['active_level']
                )

            if value['sp_type'] == 100:  # SpGpioDigitalOutput
                validate = SpGpioDigitalOutput.validate(value)
                if validate is not None:  # validate value
                    self.errors.append('Error in value {}. Port {} not append'.format(validate, port))
                    continue
                if port not in SpGpioDigitalOutput.available_ports:
                    self.errors.append('Error. Port {} not available'.format(port))
                    continue
                # add SP
                if SpGpioDigitalOutput.can_module_class is None:
                    SpGpioDigitalOutput.can_module_class = self
                self.ports[port] = SpGpioDigitalOutput(
                    port=port,
                    events=self.dict_key_to_int(value['events']),
                    open_drain=value['open_drain'],
                    active_level=value['active_level'],
                    default_value=value['default_value']
                )

            if value['sp_type'] == 101:  # SpGpioPWMOutput
                validate = SpGpioPWMOutput.validate(value)
                if validate is not None:  # validate value
                    self.errors.append('Error in value {}. Port {} not append'.format(validate, port))
                    continue
                if port not in SpGpioPWMOutput.available_ports:
                    self.errors.append('Error. Port {} not available'.format(port))
                    continue
                # add SP
                if SpGpioPWMOutput.can_module_class is None:
                    SpGpioPWMOutput.can_module_class = self
                self.ports[port] = SpGpioPWMOutput(
                    port=port,
                    events=self.dict_key_to_int(value['events']),
                    open_drain=value['open_drain'],
                    active_level=value['active_level'],
                    default_value=value['default_value']
                )

        self.generate()

    def generate(self):
        self.generate_subscribers()
        self.add_template_data(SpGpioDigitalInput.generate())
        print(self.gen_template)

        # region ### board.h ###
        board = self.board.generate()  # {'GPIOACRL': '8F888888', ..
        with open(os.path.join(self.c_project_path, 'board', 'board.h'), 'w') as f:
            tmpl = self.tmpl_env.get_template('board.tpl.h')
            f.write(
                tmpl.render(**board)
            )
        # endregion

        # region ### ldscript ###
        if self.bank > 0:
            self.bank = 1
            start_address = self.start_address_1
        else:
            start_address = self.start_address_0
        ldscript = {
            'org': '0x{:08X}'.format(start_address),
            'len': '- 0x{:X}'.format(start_address - 0x08000000)
        }
        with open(self.ldscript_path, 'w') as f:
            tmpl = self.tmpl_env.get_template('STM32F103x8.tpl.ld')
            f.write(
                tmpl.render(**ldscript)
            )
        # endregion

        # region ### halconf.h ###
        template_halconf = self.gen_template['halconf.h']
        with open(os.path.join(self.c_project_path, 'halconf.h'), 'w') as f:
            tmpl = self.tmpl_env.get_template('halconf.production.h')
            f.write(
                tmpl.render(**template_halconf)
            )
        # endregion

        # generate template from dict(gen_template)
        for template_name, template_value in self.gen_template.items():
            if template_name == 'publicators.c':
                with open(os.path.join(self.c_project_path, 'publicators.c'), 'w') as f:
                    tmpl = self.tmpl_env.get_template('publicators.tpl.c')
                    f.write(
                        tmpl.render(**template_value)
                    )
            if template_name == 'subscribers.c':
                with open(os.path.join(self.c_project_path, 'subscribers.c'), 'w') as f:
                    tmpl = self.tmpl_env.get_template('subscribers.tpl.c')
                    f.write(
                        tmpl.render(**template_value)
                    )

    def generate_subscribers(self):
        # устанавливаем для переменных шаблона значения по умолчанию
        self.add_template_data({'subscribers.c': {'pwmcfg': '', 'pwm_start': ''}})

        # ищем порты-подписчики и заполняем словарь уникальных событий
        event_dict = {}  # {unique_event: [sp_class1, sp_class2 ...] }
        sp_generate_group = {}  # {sp_type: sp_class} словарь софт-портов для групповой генерации
        for port, sp_class in self.ports.items():
            try:
                if sp_class.subscriber:
                    sp_generate_group.update({sp_class.sp_type: sp_class})
                    # генерируем групповые данные для одинаковых софт-портов
                    for event in sp_class.events:
                        if event in event_dict:
                            event_dict[event].append(sp_class.generate(event))
                        else:
                            event_dict[event] = [sp_class.generate(event)]
            except AttributeError:
                pass

        # формируем if (CanReceive.event) else
        out = ''
        for event, gen_strings in event_dict.items():
            if out == '':
                out += '\nif (CanReceive.event == {}){{\n'.format(event)
            else:
                out += 'else if (CanReceive.event == {}){{\n'.format(event)
            for gen_str in gen_strings:
                out += '  ' + gen_str + '\n'
            out += '}\n'
        self.add_template_data({'subscribers.c': {'subscriber_strings': out}})

        # групповая генерация
        for sp_type, sp_class in sp_generate_group.items():
            try:
                sp_class.generate_group()
            except AttributeError:
                pass

    def loop_event(self, port):
        pass


class SpGpioDigitalInput:
    name = "Digital input"
    sp_type = 1
    available_ports = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
    can_module_class: CanModule = None  # ссылка на класс-родитель CanModule

    def __init__(self, port, events, active_level, pull):
        self.port = port
        self.active_level = active_level
        self.pull = pull
        self.events = events
        self.board_init()

    def board_init(self):
        gpio, pin = self.can_module_class.gpio_ports_map[self.port]
        port_type = 4
        if self.pull == 1:
            port_type = 8
        pull = 1
        if self.active_level == 1 and self.pull == 1:
            pull = 0
        self.can_module_class.board.port_set(gpio, pin, port_type, pull)

    @classmethod
    def generate(cls):
        buttons = []
        template_main = Template("""
virtual_timer_t timer20ms_timer;

void timer20ms(void *p) {

  /* Restarts the timer.*/
  chSysLockFromISR();
  chVTSetI(&timer20ms_timer, (systime_t)200, timer20ms, p);  // 20 ms
  chSysUnlockFromISR();

  /* Periodic code here.*/

{% for button in buttons %}
  {{button}}
{% endfor %}
}
""")
        for port, sp_class in cls.can_module_class.ports.items():
            if sp_class.sp_type == 1:

                gpio_l, gpio_s, pin = cls.can_module_class.gpio_port_name(port)
                template = Template("""
  /* BUTTON_{{S}}{{PIN}}_START */
  static uint8_t active_cnt_{{PS}};
  static uint8_t unactive_cnt_{{PS}};
  if (palReadPad({{L}}, {{PIN}}) == 0) {
    if (unactive_cnt_{{PS}} == 0) {
      active_cnt_{{PS}}++;
      if (active_cnt_{{PS}} > 3) {
        {% for event in events -%}
        {{event}}; // send can
        {{loop_event}}; // loop event
        {% endfor %}
        unactive_cnt_{{PS}} = 5;
        active_cnt_{{PS}} = 0;
      }
    }
  }
  else {
    active_cnt_{{PS}} = 0;
    if(unactive_cnt_{{PS}} > 0){
      unactive_cnt_{{PS}}--;
    }
  }
  /* BUTTON_{{S}}{{PIN}}_END */""")
                events = []
                for event, val in sp_class.events.items():
                    if len(val) == 1:
                        events.append('can_bus_send({0}, {1[0]})'.format(event, val))
                    if len(val) == 2:
                        events.append('can_bus_send_1b({0}, {1[0]}, {1[1]})'.format(event, val))
                    if 2 < len(val) <= 9:
                        databytes_name = 'event{}_data'.format(event)
                        databytes_value = ', '.join(map(str, val[1:]))
                        databytes = 'const uint8_t ' + databytes_name + ' = {' + databytes_value + '};'
                        events.append(databytes + ' can_bus_send_d({0}, {1}, {2}, {3})'.format(
                            event, val[0], databytes_name, len(val[1:])
                        ))

                # find loop event in subscribers
                cls.can_module_class.loop_event(port)

                buttons.append(
                    template.render(S=gpio_s, L=gpio_l, PIN=pin, PS=gpio_s.lower() + str(pin), events=events)
                )

        if len(buttons):
            return {'publicators.c': {
                'timer20ms': template_main.render(buttons=buttons),
                'init_timer20ms': """  chVTSet( & timer20ms_timer, MS2ST(1000), timer20ms, NULL);"""
            }}
        else:
            return {'publicators.c': {'timer20ms': '', 'init_timer20ms': ''}}

    @staticmethod
    def validate(var):
        try:
            if var['active_level'] not in range(0, 2):
                return 'active_level'
            if var['pull'] not in range(0, 2):
                return 'pull'
            for event, val in var['events'].items():
                try:
                    if int(event) not in range(100, 45056):
                        return 'event'
                except ValueError:
                    return 'event'
                if len(val) == 0 or len(val) > 9:
                    return 'event'
                if val[0] not in range(0, 2):
                    return 'event'
                for item in val[1:]:
                    if item not in range(0, 256):
                        return 'event'
        except KeyError as err:
            return err
        return None

    def save(self):
        ret = {
            'sp_type': self.sp_type,
            'active_level': self.active_level,
            'pull': self.pull,
            'events': self.events
        }
        return ret


class SpGpioDigitalOutput:
    name = "Digital output"
    sp_type = 100
    subscriber = True
    available_ports = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
    can_module_class: CanModule = None  # ссылка на класс-родитель CanModule

    def __init__(self, port, events, active_level, open_drain, default_value):
        self.port = port
        self.events = events
        self.active_level = active_level
        self.open_drain = open_drain
        self.default_value = default_value
        self.board_init()

    def board_init(self):
        gpio, pin = self.can_module_class.gpio_ports_map[self.port]
        port_type = 2
        if self.open_drain:
            port_type = 6
        self.can_module_class.board.port_set(gpio, pin, port_type, self.default_value)

    @staticmethod
    def validate(var):
        try:
            if var['active_level'] not in range(0, 2):
                return 'active_level'
            if var['default_value'] not in range(0, 2):
                return 'default_value'
            if var['open_drain'] not in range(0, 2):
                return 'open_drain'
            for event, val in var['events'].items():
                try:
                    if int(event) not in range(100, 45056):
                        return 'event'
                except ValueError:
                    return 'event'
                if len(val) != 1:
                    return 'event'
                if val[0] not in range(0, 2):
                    return 'event'
        except KeyError as err:
            return err
        return None

    def generate(self, event):
        value = 0
        value_calc = self.events[event][0] + self.active_level
        if value_calc == 0:
            value = 1
        elif value_calc == 1:
            value = 0
        elif value_calc == 2:
            value = 1

        return 'set_gpio_port({}, CanReceive.databit, {});'.format(self.port, value)

    def save(self):
        return {
            'sp_type': self.sp_type,
            'active_level': self.active_level,
            'open_drain': self.open_drain,
            'events': self.events
        }


class SpGpioPWMOutput:
    name = "PWM output"
    sp_type = 101
    subscriber = True
    available_ports = [9, 10, 13, 14, 15, 16, 17, 18]
    # {port: [timer, timer_ch]}
    channels = {9: [3, 0], 10: [3, 1], 13: [3, 2], 14: [3, 3], 15: [4, 0], 16: [4, 1], 17: [4, 2], 18: [4, 3]}
    can_module_class: CanModule = None  # ссылка на класс-родитель CanModule

    def __init__(self, port, events, active_level, default_value, open_drain):
        self.port = port
        self.events = events
        self.active_level = active_level
        self.default_value = default_value
        self.open_drain = open_drain
        self.board_init()

    def board_init(self):
        gpio, pin = self.can_module_class.gpio_ports_map[self.port]
        port_type = 0xB
        if self.open_drain:
            port_type = 0xF
        self.can_module_class.board.port_set(gpio, pin, port_type)

    @staticmethod
    def validate(var):
        try:
            if var['active_level'] not in range(0, 2):
                return 'active_level'
            if var['default_value'] not in range(0, 101):
                return 'default_value'
            if var['open_drain'] not in range(0, 2):
                return 'open_drain'
            for event, val in var['events'].items():
                try:
                    if int(event) not in range(100, 45056):
                        return 'event'
                except ValueError:
                    return 'event'
                if len(val) != 2:
                    return 'event'
                if val[0] not in range(0, 101) or val[1] not in range(0, 101):
                    return 'event'
        except KeyError as err:
            return err
        return None

    def generate(self, event):
        return ('set_pwm_port({}, {}, {}, {});'.format(
            self.channels[self.port][0],
            self.channels[self.port][1],
            self.events[event][0],
            self.events[event][1]
        ))

    def generate_group(self):

        used_gpio = {}  # port: [gpio, pin]
        for port, sp_class in self.can_module_class.ports.items():
            if sp_class.sp_type == 101:
                self.can_module_class.add_template_data({'halconf.h': {'HAL_USE_PWM': 'TRUE'}})
                self.can_module_class.add_template_data({'subscribers.c': {'HAL_USE_PWM': 'TRUE'}})
                used_gpio[port] = self.can_module_class.gpio_ports_map[port]

        tim3_gpio = [[0, 6], [0, 7], [1, 0], [1, 1]]
        tim4_gpio = [[1, 6], [1, 7], [1, 8], [1, 9]]
        tim3 = ['PWM_OUTPUT_DISABLED'] * 4
        tim3_f = 0
        tim4 = tim3[:]
        tim4_f = 0
        for port, gpio in used_gpio.items():
            for i, t_gpio in enumerate(tim3_gpio):
                if gpio == t_gpio:
                    if self.can_module_class.ports[port].active_level:
                        tim3[i] = 'PWM_OUTPUT_ACTIVE_HIGH'
                    else:
                        tim3[i] = 'PWM_OUTPUT_ACTIVE_LOW'
                    tim3_f = 1
            for i, t_gpio in enumerate(tim4_gpio):
                if gpio == t_gpio:
                    if self.can_module_class.ports[port].active_level:
                        tim4[i] = 'PWM_OUTPUT_ACTIVE_HIGH'
                    else:
                        tim4[i] = 'PWM_OUTPUT_ACTIVE_LOW'
                    tim4_f = 1
        # add to define subscribers
        cfg = Template("""static PWMConfig pwmcfg{{cfg_n}} = {
          10000,                            /* 10kHz PWM clock frequency.   */
          100,                              /* Initial PWM period 10 mS    */
          NULL,
          {
           { {{port[0]}}, NULL},    // CH1 A6 or B6
           { {{port[1]}}, NULL},    // CH2 A7 or B7
           { {{port[2]}}, NULL},    // CH3 B0 or B8
           { {{port[3]}}, NULL}    // CH4 B1 or B9
          },
          0,
          0,
        #if STM32_PWM_USE_ADVANCED
          0
        #endif
        };

        """)

        pwmcfg = []
        pwm_start = []
        pwm_default = []

        if tim3_f:
            pwmcfg.append(cfg.render(port=tim3, cfg_n=0))
            pwm_start.append('pwmStart(&PWMD3, &pwmcfg0);')
        if tim4_f:
            pwmcfg.append(cfg.render(port=tim4, cfg_n=1))
            pwm_start.append('pwmStart(&PWMD4, &pwmcfg1);')



        # default values
        for port, sp_class in self.can_module_class.ports.items():
            if sp_class.sp_type == 101:
                pwm_default.append(
                    'pwmEnableChannel(&PWMD{0[0]}, {0[1]}, PWM_PERCENTAGE_TO_WIDTH(&PWMD{0[0]}, {1}));'.format(
                        sp_class.channels[sp_class.port], sp_class.default_value * 100
                    )
                )

        self.can_module_class.add_template_data({
            'subscribers.c': {
                'pwmcfg': pwmcfg,
                'pwm_start': pwm_start,
                'pwm_default': pwm_default
            }
        })

    def save(self):
        return {
            'sp_type': self.sp_type,
            'active_level': self.active_level,
            'default_value': self.default_value,
            'open_drain': self.open_drain,
            'events': self.events
        }


if __name__ == "__main__":
    can_module_class = CanModule(
        r'C:\ChibiStudio\workspace176\can_module_online_gen',
        r'C:\ChibiStudio\chibios176\os\common\startup\ARMCMx\compilers\GCC\ld\STM32F103x8_csh.ld',
        0, 200
    )

    can_module_class.add_sp(
        """{
  "1": {
    "events": { "700": [1]},
    "active_level": 0,
    "pull": 1,
    "sp_type": 1
  },
  "2": {
    "events": { "702": [1]},
    "active_level": 1,
    "pull": 1,
    "sp_type": 1
  },
  "3": {
    "events": { "700": [1], "800": [0]},
    "active_level": 0,
    "default_value": 0,
    "open_drain": 1,
    "sp_type": 100
  },
  "4": {
    "events": { "700": [1], "801": [0]},
    "active_level": 0,
    "default_value": 0,
    "open_drain": 1,
    "sp_type": 100
  },
  "18": {
    "active_level": 1,
    "default_value": 0,
    "events": {"129": [0, 100], "800": [1, 33]},
    "open_drain": 1,
    "sp_type": 101
  }
}"""
    )

    if can_module_class.errors:
        print(can_module_class.errors)
