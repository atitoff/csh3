import zmq
import zmq.asyncio
import msgpack
import asyncio


class CanZmq:
    def __init__(self, zmq_ctx, ip='192.168.1.201', zmq_sub=5432, zmq_req=5560, zmq_stat=5561):
        self._zmq_ctx = zmq_ctx
        self._req = self._zmq_ctx.socket(zmq.REQ)
        self._stat = self._zmq_ctx.socket(zmq.REQ)
        self.sub = self._zmq_ctx.socket(zmq.SUB)
        self._sub_list = []
        self._zmq_sub = 'tcp://{}:{}'.format(ip, zmq_sub)
        self._zmq_req = 'tcp://{}:{}'.format(ip, zmq_req)
        self._zmq_stat = 'tcp://{}:{}'.format(ip, zmq_stat)
        self.sub.connect(self._zmq_sub)
        self._req.connect(self._zmq_req)
        self._stat.connect(self._zmq_stat)

    def unsubscribe(self):
        for item in self._sub_list:
            self.sub.setsockopt(zmq.UNSUBSCRIBE, item)
        self._sub_list = []

    def subscribe(self, sub_string: bytes):
        self.sub.setsockopt(zmq.SUBSCRIBE, sub_string)
        self._sub_list.append(sub_string)

    async def send_can_msg(self, msg: list):
        await self._req.send(b"\x00" + msgpack.packb(msg))
        return await self._req.recv()  # wait for msg to be ready

    async def get_module_without_id(self):
        ret = []
        await self._stat.send(b"\x03")
        for key, val in msgpack.unpackb(await self._stat.recv()).items():
            if len(val[1]) == 2:
                ret.append(key)
        return ret
