import asyncio
import zmq.asyncio
import msgpack
import math
import time
import progressbar
from intelhex import IntelHex
import logging


class Crc32Stm32:
    """ Calculate stm32 crc32
    crc - побайтно, crc32 по 4-х байтовому слову """

    def __init__(self, poly=0x04C11DB7):
        self._poly = poly
        self._custom_crc_table = {}
        self._generate_crc32_table()

    def _generate_crc32_table(self):
        for i in range(256):
            c = i << 24
            for j in range(8):
                c = (c << 1) ^ self._poly if (c & 0x80000000) else c << 1
                self._custom_crc_table[i] = c & 0xffffffff

    def crc32(self, buf):
        _crc = 0xffffffff
        for integer in buf:
            b = self._int_to_bytes(integer)
            for byte in b:
                _crc = ((_crc << 8) & 0xffffffff) ^ self._custom_crc_table[(_crc >> 24) ^ byte]
        return _crc

    def crc32_32b(self, buf):
        """ calculate byte array as 32 bit
        количество входных элементов массива должно быть кратно 4-м
        """
        _crc = 0xffffffff
        for i in range(0, len(buf), 4):
            try:
                b = [buf[i + 3], buf[i + 2], buf[i + 1], buf[i]]
                for byte in b:
                    _crc = ((_crc << 8) & 0xffffffff) ^ self._custom_crc_table[(_crc >> 24) ^ byte]
            except IndexError:
                pass
        return _crc

    @staticmethod
    def _int_to_bytes(i):
        return [(i >> 24) & 0xFF, (i >> 16) & 0xFF, (i >> 8) & 0xFF, i & 0xFF]


class WriteProgramBank:
    """
    Output msg to external function (may be coroutine)
    for example:
    def progress(value):
        try:
            if len(value) == 2:
                print(value[1])
        except TypeError:
            if int(value * 100) > 0:
                bar.update(int(value * 100))
            if value == 1:
                bar.finish()
    """

    def __init__(self, zmq_ctx, zmq_sub, zmq_req,
                 logging, module_id=0, module_uid=0, page_size=1024, progress=None):
        self._zmq_ctx = zmq_ctx
        self._zmq_sub = zmq_sub
        self._zmq_req = zmq_req
        self._page_size = page_size
        self._module_id = module_id
        self._module_uid = module_uid
        self._fw_data = list()
        self._fw_page_cnt = 0
        self._sock = self._zmq_ctx.socket(zmq.REQ)
        self._sub = self._zmq_ctx.socket(zmq.SUB)
        self._sub_list = []
        self._sub.connect(self._zmq_sub)
        self._sock.connect(self._zmq_req)
        self.progress = progress
        self._logging = logging

    def unsubscribe(self):
        for item in self._sub_list:
            self._sub.setsockopt(zmq.UNSUBSCRIBE, item)
        self._sub_list = []

    def subscribe(self, sub_string):
        self._sub.setsockopt(zmq.SUBSCRIBE, sub_string)
        self._sub_list.append(sub_string)

    async def send_can_msg(self, msg):
        await self._sock.send(b"\x00" + msgpack.packb(msg))
        return await self._sock.recv()  # waits for msg to be ready

    async def find_module(self):
        self.unsubscribe()
        self.subscribe(b'EV45105')
        await self.send_can_msg([45104, 0, self._module_id])
        try:
            msg = await asyncio.wait_for(self._sub.recv(), timeout=2)  # waits for msg to be ready 2 s
        except asyncio.TimeoutError:
            return None
        msg = msgpack.unpackb(msg[7:])[1]
        if msg[0] == 45105 and msg[2] == self._module_id:
            return msg
        else:
            return None

    async def send_prog_data(self, msg, sync):
        await self._sock.send(b"\x00" + msgpack.packb(self.prog_data2can_msg(msg, sync)))
        return await self._sock.recv()  # waits for msg to be ready

    @staticmethod
    def prog_data2can_msg(msg, sync):
        """ Преобразование 1-11 битного PROG_DATA в стандартное can_msg """
        if len(msg) == 1:
            msg = msg + [0, 0]
        if len(msg) == 2:
            msg = msg + [0]
        msg.insert(0, 24 + sync)
        a = int.from_bytes(msg[0:4], byteorder='big')
        out = list()
        out.append(a >> 13)
        out.append(a >> 12 & 1)
        out.append(a & 0xFFF)
        # for i in msg[4:]:
        out = out + msg[4:12]
        return out

    def crc32_pages(self):
        """ Подсчет CRC32 всех непустых страниц прошивки """
        add_bytes = self._fw_page_cnt * self._page_size - len(self._fw_data)
        crc = Crc32Stm32()
        return crc.crc32_32b(self._fw_data + [0xFF] * add_bytes)

    async def _reset_ready_prog(self):
        for i in range(3):
            await self.send_can_msg([45072, 0, 0])
            await asyncio.sleep(0.1)

    async def to_progress(self, data):
        if self.progress:
            if asyncio.iscoroutinefunction(self.progress):
                await self.progress(data)
            else:
                self.progress(data)

    async def write_fw(self, fw_file):
        start_time = time.time()
        retrain_cnt = 0
        retrain_serial_error_cnt = 0
        crc_error_cnt = 0

        # определяем режим прошивки ONLINE или BOOTLOADER
        if self._module_id > 200:
            online_mode = True
        elif self._module_uid > 0:
            online_mode = False
        else:
            return [False, 'Write mode ONLINE or BOOTLOADER undefined']

        crc = Crc32Stm32()

        try:
            ih = IntelHex(fw_file)
        except FileNotFoundError as e:
            self._logging.error(e)
            return

        start_address_list = list(ih.addresses()[0].to_bytes(4, byteorder='little'))
        module_uid_list = list(self._module_uid.to_bytes(4, byteorder='little'))

        self._fw_data = list(ih.tobinarray())

        # если длина нечетная добавляем байт 0xFF
        if len(self._fw_data) & 1:
            self._fw_data += [0xFF]

        self._fw_page_cnt = math.ceil(len(self._fw_data) / self._page_size)

        await self._reset_ready_prog()

        # ждем ответа 45081
        self.unsubscribe()
        self.subscribe(b'EV45081')

        if online_mode:
            await self.send_can_msg(
                [45080, 0, self._module_id] + start_address_list + [int(self._fw_page_cnt)]
            )
        else:
            await self.send_can_msg([45080, 0, self._fw_page_cnt] + start_address_list + module_uid_list)
        # после отправки модуль должен сам проверить self._fw_page_cnt
        # на превышение и если оно есть не стирать страницы и обнулить READY_PROG

        try:
            msg = await asyncio.wait_for(self._sub.recv(), timeout=2)  # waits for msg to be ready 1 s
        except asyncio.TimeoutError:
            if online_mode:
                return [0, 'module {} is not found on the network'.format(self._module_id)]
            else:
                return [0, 'module UID: {:X} is not found on the network'.format(self._module_uid)]

        msg = msgpack.unpackb(msg[7:])[1]

        self.unsubscribe()
        self.subscribe(b'EV45083')

        if online_mode:
            if msg[0] == 45081 and msg[2] == self._module_id:
                if len(msg) > 3:
                    await self._reset_ready_prog()
                    if msg[3] == 1:
                        return [0, 'FW too big']
                    else:
                        return [0, 'Start address mismatch']
            else:
                return
        else:  # offline mode
            if msg[0] == 45081:
                if len(msg) > 7:
                    await self._reset_ready_prog()
                    if msg[7] == 1:
                        return [0, 'FW too big']
                    else:
                        return [0, 'Start address mismatch']
            else:
                return

        i = 0  # порядковый номер байта в self._fw_data
        while i < len(self._fw_data):
            block_88 = self._fw_data[i:i + 88]
            await self.to_progress(i / len(self._fw_data))
            block88_number = i // 88
            block88_number_list = list(block88_number.to_bytes(2, byteorder='little'))
            crc16 = crc.crc32(block_88)

            await asyncio.sleep(0.01)
            msg45082 = [45082, 0, self._module_id] + block88_number_list + [crc16 & 0xFF, crc16 >> 8 & 0xFF, len(block_88)]
            await self.send_can_msg(msg45082)

            n = 0
            sync = 0
            while n < len(block_88):
                block_11 = block_88[n:n + 11]
                # print(block_11)
                await asyncio.sleep(0.01)
                await self.send_prog_data(block_11, sync)

                sync += 1
                n += 11

            # ждем сообщения 45083
            try:
                msg = await asyncio.wait_for(self._sub.recv(), timeout=0.5)  # waits for msg to be ready 0.5 s
                msg = msgpack.unpackb(msg[7:])[1]
                if msg[1]:  # блок принят без ошибок
                    i += 88
                    retrain_serial_error_cnt = 0
                else:
                    crc_error_cnt += 1
                    await asyncio.sleep(0.1)
                    print('block error')

            except asyncio.TimeoutError:
                retrain_cnt += 1
                print('retrain')

                if retrain_serial_error_cnt > 5:
                    return [0, 'Communication error']
                else:
                    retrain_serial_error_cnt += 1

        await self.to_progress(1)

        self.unsubscribe()
        self.subscribe(b'EV45085')

        fw_crc = self.crc32_pages()

        await self.send_can_msg(
            [45084, 0, self._module_id, fw_crc & 0xFF, fw_crc >> 8 & 0xFF, fw_crc >> 16 & 0xFF, fw_crc >> 24 & 0xFF]
        )

        msg = await asyncio.wait_for(self._sub.recv(), timeout=5)  # waits for msg to be ready 5 s
        msg = msgpack.unpackb(msg[7:])[1]
        if msg[1] == 1:  # success
            runtime = time.time() - start_time
            send_bytes = len(self._fw_data)
            write_speed = len(self._fw_data) / runtime
            return_dict = dict()
            for i in ('runtime', 'retrain_cnt', 'crc_error_cnt', 'send_bytes', 'write_speed', 'fw_crc', 'online_mode'):
                return_dict[i] = locals()[i]
            return [1, return_dict]
        else:
            return [0, 'CRC error']


if __name__ == "__main__":

    ctx = zmq.asyncio.Context()
    loop = zmq.asyncio.ZMQEventLoop()
    asyncio.set_event_loop(loop)

    bar = progressbar.ProgressBar(max_value=100)


    def progress(value):
        try:
            if len(value) == 2:
                print(value[1])
        except TypeError:
            if int(value * 100) > 0:
                bar.update(int(value * 100))
            if value == 1:
                bar.finish()


    async def write_program_bank():
        writer = WriteProgramBank(
            zmq_ctx=ctx,
            zmq_sub="tcp://192.168.1.201:5432",
            zmq_req="tcp://192.168.1.201:5560",
            module_id=222,
            module_uid=0x7FB787E9,
            # module_uid=0x7FB787E9,
            progress=progress,
            logging=logging
        )

        print(await writer.write_fw(r'C:\ChibiStudio\workspace161\can_module_online_gen\build\ch.hex'))


    loop.run_until_complete(write_program_bank())
    loop.close()
