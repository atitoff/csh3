import asyncio
import glob
import json
import logging
import os
import re
import subprocess
import sys

import msgpack
import progressbar
import zmq
import zmq.asyncio

from CSHLib import CodeGenerator, Bootloader
from CSHLib.CanZmq import CanZmq


class ConsoleWriter:
    def __init__(self, settings, js):
        self.settings = settings
        self.chibios_path = ''
        self.set_path()
        self.js = js
        self.module_id = self.get_module_id()
        self.progressbar = progressbar.ProgressBar(max_value=100)
        # start loop
        self.ctx = zmq.asyncio.Context()
        self.loop = zmq.asyncio.ZMQEventLoop()
        asyncio.set_event_loop(self.loop)
        # run main program
        self.loop.run_until_complete(self.run())
        self.loop.close()

    def set_path(self):
        chdir = os.path.normpath(os.path.join(os.getcwd(), '..', 'chibios161'))
        if os.path.isdir(chdir):
            self.chibios_path = os.path.normpath(os.path.join(os.getcwd(), '..'))
        else:
            self.chibios_path = self.settings.chibios_path

    def progress(self, value):
        try:
            if len(value) == 2:
                print(value[1])
        except TypeError:
            if int(value * 100) > 0:
                self.progressbar.update(int(value * 100))
            if value == 1:
                self.progressbar.finish()

    def get_module_id(self):
        module_id_str = os.path.splitext(os.path.basename(sys.argv[0]))[0]
        try:
            module_id = int(module_id_str)
            if module_id not in range(200, 4096):
                print('module_id: {} должен быть в диапазоне 200..4095'.format(module_id))
                exit()
            else:
                return module_id
        except ValueError:
            print('Неправильный module_id: {}'.format(module_id_str))
            exit()

    def backup_js(self):
        backup_folder_path = os.path.join(os.path.dirname(sys.argv[0]), 'backup', str(self.module_id))
        # find exists *.json files
        list_of_files = glob.glob(backup_folder_path + '/*.json')
        if len(list_of_files) > 0:
            latest_file = max(list_of_files, key=os.path.getctime)
            # open json
            with open(latest_file, 'r') as f:
                json_old = json.load(f)
            # compare json
            a, b = json.dumps(json_old, sort_keys=True), json.dumps(json.loads(self.js), sort_keys=True)
            if a == b:
                return
            last_name = os.path.splitext(os.path.basename(latest_file))[0]
            file_number = int(last_name.split('_')[1])
            new_filename = '{}_{:05}.json'.format(self.module_id, file_number + 1)
        else:
            os.makedirs(backup_folder_path, exist_ok=True)
            new_filename = '{}_{:05}.json'.format(self.module_id, 1)

        filename = os.path.join(backup_folder_path, new_filename)
        with open(filename, 'w') as f:
            f.write(self.js)

    async def run(self):
        print('Ищем модуль {} на шине CAN'.format(self.module_id))
        conn = CanZmq(self.ctx, ip=self.settings.loggerd_ip_address)
        conn.unsubscribe()
        conn.subscribe(b'EV45105')
        await conn.send_can_msg([45104, 0, self.module_id])
        try:
            msg = await asyncio.wait_for(conn.sub.recv(), timeout=2)  # waits for msg to be ready 2s
            msg = msgpack.unpackb(msg[7:])[1]
            if msg[2] != self.module_id:
                msg = None
        except asyncio.TimeoutError:
            msg = None

        if msg:
            current_program_bank = msg[10] & 1
            print('Модуль найден. Текущий банк: {}'.format(current_program_bank))
            write_program_bank = 1
            if current_program_bank == 1:
                write_program_bank = 0
            if await self.compile_fw(write_program_bank):
                await self.write_fw()
        else:
            uid_list = await conn.get_module_without_id()
            print('Модуль не найден.\n\r Список модулей без ID')
            for i, item in enumerate(uid_list):
                print('{}: 0x{:08X}'.format(i + 1, item))
            key_input = input('В модуль с каким UID записать FW? Введите "0" для выхода: ')
            if key_input == '0':
                exit()
            try:
                module_uid = uid_list[int(key_input) - 1]
                print('Выбран модуль с UID: 0x{:08X}'.format(module_uid))
                if await self.compile_fw(0):
                    await self.write_fw(module_uid=module_uid)
            except (IndexError, ValueError):
                exit()

    async def compile_fw(self, bank=0):
        c_project_path = os.path.join(self.chibios_path, self.settings.project_path)
        module = CodeGenerator.CanModule(
            c_project_path=c_project_path,
            ldscript_path=os.path.join(self.chibios_path, self.settings.ld_script_path),
            bank=bank,
            module_id=self.module_id
        )

        module.add_sp(self.js)

        if len(module.errors) > 0:
            print('Errors:')
            for error in module.errors:
                print(error)
            exit()

        module.generate()

        os.environ["PATH"] += os.pathsep + os.path.join(self.chibios_path, r'tools\gnutools\bin')
        os.environ["PATH"] += os.pathsep + os.path.join(
            self.chibios_path, r'tools\GNU Tools ARM Embedded\7.0 2017q4\arm-none-eabi\bin'
        )
        os.environ["PATH"] += os.pathsep + os.path.join(
            self.chibios_path, r'tools\GNU Tools ARM Embedded\7.0 2017q4\bin'
        )

        make_last_line = ''
        p = subprocess.Popen('make -j4 all', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                             cwd=c_project_path, bufsize=1)
        print('Compile bank {}'.format(bank), end='', flush=True)
        compile_ret_re = re.compile(r'^(\d{4,})\s+(\d{3,})')
        for line in iter(p.stdout.readline, b''):
            ln = line.strip().decode()
            compile_ret = compile_ret_re.findall(ln)
            if ln[:4] in ['Comp', 'Crea', 'Link']:
                print('.', end='', flush=True)
            elif compile_ret:
                print('size {} B'.format(int(compile_ret[0][0])+int(compile_ret[0][1])))
            make_last_line = ln
        p.stdout.close()
        p.wait()
        if make_last_line == 'Done':
            return True
            pass
        else:
            exit()

    async def write_fw(self, module_uid=0):
        if module_uid > 0:
            module_id = 0
        else:
            module_id = self.module_id

        writer = Bootloader.WriteProgramBank(
            zmq_ctx=self.ctx,
            zmq_sub="tcp://{}:{}".format(self.settings.loggerd_ip_address, self.settings.zmq_sub),
            zmq_req="tcp://{}:{}".format(self.settings.loggerd_ip_address, self.settings.zmq_req),
            module_id=module_id,
            module_uid=module_uid,
            progress=self.progress,
            logging=logging
        )

        writer_ret = await writer.write_fw(
            os.path.join(self.chibios_path, self.settings.project_path, r'build\ch.hex')
        )
        if writer_ret[0] == 1:
            print('Модуль записан успешно')
            print(writer_ret[1])
            # делаем бекап json
            self.backup_js()
        else:
            print("Ошибка записи модуля: ", writer_ret)
        input('\nPress Enter for exit')
